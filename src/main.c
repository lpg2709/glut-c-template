// System Headers
#include <stdio.h>
#include <stdlib.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#define mWidth 800
#define mHeight 600

void renderFunction();

int main(int argc, char ** argv){

	printf("[INFO] Starting OpenGL main loop.");

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(mWidth, mHeight);
    glutCreateWindow("glut-c-template");
    // Display Callback Function
    glutDisplayFunc(&renderFunction);
    // Start main loop
    fprintf(stderr, "OpenGL %s\n", glGetString(GL_VERSION));
    glutMainLoop();
	printf("[INFO] Exit OpenGL main loop.");

    return EXIT_SUCCESS;
}

void renderFunction(){
	printf("[INFO] Running loop.");
    // Clear the current output buffer
    glClear(GL_COLOR_BUFFER_BIT);

    // Rotate 10 degrees counterclockwise around z axis
    glRotated(10, 0, 0, 1);

    // Set the current color (RGB) drawing to blue
    glColor3f(0.0, 0.0, 1.0);

    // Start polygon
    glBegin(GL_POLYGON);
    glVertex3f(-0.5, -0.5, 0);
    glVertex3f( 0.5, -0.5, 0);
    glVertex3f( 0.5,  0.5, 0);
    glVertex3f(-0.5,  0.5, 0);
    // End polygon
    glEnd();

    glFlush();
}
