# glut-c-template

This is my person configuration for develop OpenGL applications in C using [freeglut](https://freeglut.sourceforge.net/).

## Libs

- [freeglut-3.4.0](https://freeglut.sourceforge.net/) - freeglut is a free-software/open-source alternative to the OpenGL Utility Toolkit (GLUT) library.

## How use

Clone the repository.

Go to CMakeLists.txt and change the `project()` name to the name of your project.

```sh
mkdir build
cmake ..
cmake --build .
```

## Why not use submodules

Because for this project I preferred to use everything in a single repository, without depending on a submodule. Just a personal choice.

## References

This template is based on [opengl-c-template](https://gitlab.com/lpg2709/opengl-c-template)

## License

[MIT](LICENSE)
